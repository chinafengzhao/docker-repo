# 一、系统选择

关于最基础的底层镜像, 通常大多数我们只有三种选择: Alpine、Debian、CentOS; 这三者中对于运维最熟悉的一般为 CentOS, 但是很不幸的是 CentOS 后续已经不存在稳定版, 关于它的稳定性问题一直是个谜一样的问题; 这是一个仁者见仁智者见智的问题, 我个人习惯是能不用我绝对不用 😆.

排除掉 CentOS 我们只讨论是 Alpine 还是 Debian; 从镜像体积角度考虑无疑 Alpine 完胜, 但是 Alpine 采用的是 musl 的 C 库, 在某些深度依赖 glibc 的情况下可能会有一定兼容问题. 当然关于深度依赖 glibc 究竟有多深度取决于具体应用, 就目前来说我个人也只是遇到过 Alpine 官方源中的 OpneJDK 一些字体相关的 BUG.

综合来说, 我个人的建议是如果应用深度依赖 glibc, 比如包含一些 JNI 相关的代码, 那么选择 Debian 或者说基于 Debian 的基础镜像是一个比较稳的选择; 如果没有这些重度依赖问题, 那么在考虑镜像体积问题上可以选择使用 Alpine. 事实上 OpneJDK 本身体积也不小, 即使使用 Alpine 版本, 再安装一些常用软件后也不会小太多, 所以我个人习惯是使用基于 Debian 的基础镜像.

# 二、JDK OR JRE

很多人似乎从不区分 JDK 与 JRE, 所以要确定这事情需要先弄明白 JDK 和 JRE 到底是什么:

- JDK: Java Development Kit
- JRE: Java Runtime Environment

JDK 是一个开发套件, 它会包含一些调试相关的工具链, 比如 javac、jps、jstack、jmap 等命令, 这些都是为了调试和编译 Java 程序所必须的工具, 同时 JDK 作为开发套件是包含 JRE 的; 而 JRE 仅为 Java 运行时环境, 它只包含 Java 程序运行时所必须的一些命令以及依赖类库, 所以 JRE 会比 JDK 体积更小、更轻量.

如果只需要运行 Java 程序比如一个 jar 包, 那么 JRE 足以; 但是如果期望在运行时捕获一些信息进行调试, 那么应该选择 JDK. 我个人的习惯是为了解决一些生产问题, 通常选择直接使用 JDK 作为基础镜像, 避免一些特殊情况还需要挂载 JDK 的工具链进行调试. 当然如果没有这方面需求, 且对镜像体积比较敏感, 那么可以考虑使用 JRE 作为基础镜像.


# JDK的选择

OracleJDK 还是 OpenJDK

针对于这两者的选择, 取决于一个最直接的问题: 应用代码中是否有使用 Oracle JDK 私有 API.

通常 「使用这些私有 API」 指的是引入了一些 com.sun.* 包下的相关类、接口等, 这些 API 很多是 Oracle JDK 私有的, 在 OpneJDK 中可能完全不包含或已经变更. 所以如果代码中包含相关调用则只能使用 Oracle JDK.

值得说明的是很多时候使用这些 API 并不是真正的业务需求, 很可能是开发在导入包时 “手滑” 并且凑巧被导入的 Class 等也能实现对应功能; 对于这种导入是可以被平滑替换的, 比如换成 Apache Commons 相关的实现. 还有一种情况是开发误导入后及时发现了, 但是没有进行代码格式化和包清理, 这是会在代码头部遗留相关的 import 引用, 而 Java 是允许存在这种无用的 import 的; 针对这种只需要重新格式化和优化导入即可.

Tips: IDEA 按 Option + Command + L(格式化) 还有 Control + Option + O(自动优化包导入).

当没有办法必须使用 Oracle JDK 时, 推荐自行下载 Oracle JDK 压缩包并编写 Dockerfile 创建基础镜像. 但是这会涉及到一个核心问题: Oracle JDK 一般不提供历史版本, 所以如果要考虑未来的重新构建问题, 建议保留好下载的 Oralce JDK 压缩包


目前 AdoptOpenJDK 已经合并到 Eclipse Foundation, 现在叫做 Eclipse Adoptium; 所以如果想要使用 AdoptOpenJDK 镜像, Docker Hub 中应该使用 eclipse-temurin 用户下的相关镜像.



# 信号量传递

在很多原始的 Java 项目中通常会存在一个启动运行脚本, 这些脚本可能是自行编写的, 也可能是一些比较老的 Tomcat 启动脚本等; 当我们使用脚本启动并且没有合理的调整 Dockerfile 时就会出现信号无法正确传递的问题; 例如下面的错误示范:

**entrypoint.bad.sh: 负责启动**

```bash
#!/usr/bin/env bash
java -jar /SpringBootGracefulShutdownExample-0.0.1-SNAPSHOT.jar
```

Dockerfie.bad: 使用 bash 启动脚本, 这会导致终止信号无法传递
```Dockerfile
ROM eclipse-temurin:11-jdk

COPY entrypoint.bad.sh /
COPY target/SpringBootGracefulShutdownExample-0.0.1-SNAPSHOT.jar /

# 下面几种种方式都无法转发信号
#CMD /entrypoint.bad.sh
#CMD ["/entrypoint.bad.sh"]
CMD ["bash", "/entrypoint.bad.sh"]
```









